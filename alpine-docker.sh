# remove dirs for failed mounts
rmdir /sys/fs/cgroup/cpu && rmdir /sys/fs/cgroup/cpuacct && rmdir /sys/fs/cgroup/net_cls && rmdir /sys/fs/cgroup/net_prio

# mount missing cgroups (Ubuntu style)
mkdir "/sys/fs/cgroup/cpu,cpuacct"
mount -n -t cgroup -o "nodev,noexec,nosuid,cpu,cpuacct" "cpu,cpuacct" "/sys/fs/cgroup/cpu,cpuacct"
ln -s "cpu,cpuacct" /sys/fs/cgroup/cpu
ln -s "cpu,cpuacct" /sys/fs/cgroup/cpuacct

mkdir "/sys/fs/cgroup/net_cls,net_prio"
mount -n -t cgroup -o "nodev,noexec,nosuid,net_cls,net_prio" "net_cls,net_prio" "/sys/fs/cgroup/net_cls,net_prio"
ln -s "net_cls,net_prio" /sys/fs/cgroup/net_cls
ln -s "net_cls,net_prio" /sys/fs/cgroup/net_prio

# mount systemd cgroup (Alpine mounts openrc, but Docker requires systemd...)
# (based on hint at https://k9s.hatenablog.jp/entry/2019/06/16/075741)
mkdir /sys/fs/cgroup/systemd
mount -n -t cgroup -o none,name=systemd cgroup /sys/fs/cgroup/systemd
